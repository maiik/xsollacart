// //Load m-utils
// (function () {
//   var s = document.createElement('script');
//   s.type = 'text/javascript';
//   s.async = false;
//   s.src = 'http://xsolla.maiik.ru/xsolla_cart/m-utils.js';
//   var head = document.getElementsByTagName('head')[0];
//   head.appendChild(s);
// })();






/*******************************************************/
/* Shop
 * Shop принимает settings
 * собирает кнопки с экрана по дата атрибуту
 * (позже надло переделать, чтобы создавались shop-итемы
 * и сами создавали кнопки)
 * создает Cart
 *
 * Cart
 * принимает настройки PS и другие
 * создает массив для PS
 * удаляет, добавляет, меняет объекты
 *
 * RenderedGoood
 * Объект в корзине
 * Листнеры на клики, меняет дом своего элемента
/*******************************************************/



var debug = true;


function Shop(settings) {
  this._settings = settings;
  this.shopData = settings['shopData'];
  this._data = this.buildData(settings['shopData']);
  // this.cats = this.getCats();
  this.buttons = [];
  this.cart = this.cart || new Cart(this);
  this.addButtonsArr = $(this._settings['cartAddBut']);
  if (this._settings['noDynamicStoreftont']) { //Simple Plain Shop with cart
    this.collectButtons();
  } else { //Shop-shop
    this.defaultGrids = [];
    this._settings['defaultGrids'].forEach(function (item, i) {
      this.defaultGrids.push(new ShopGrid(this, item['where'], item['typeName'], item['typeValue']));
    }.bind(this));
    // this.collectButtons(); //TODO: temp
  }
}


Shop.prototype.buildData = function (data) {
  var jsonAllGoods = {};
  var thiss = this;
  data.forEach(function (grp) {
    var groupName = grp['name'];
    var groupId = grp['id'];
    var groupItems = grp['virtual_items'];
    groupItems.forEach(function(item) {
      if (jsonAllGoods[item.sku]) {
        jsonAllGoods[item.sku]['group'].push(groupName);
        jsonAllGoods[item.sku]['groupId'].push(groupId);
      } else {
        jsonAllGoods[item.sku] = item;
        jsonAllGoods[item.sku]['group'] = [groupName];
        jsonAllGoods[item.sku]['groupId'] = [groupId];
      }

      //TODO: улучшить мердж кастом картинок
      try {
        jsonAllGoods[item.sku]['image_url_custom'] = thiss._settings['customPics'][item.sku]['image_url_custom'];
      } catch (e) { }
      try {
        jsonAllGoods[item.sku]['image_url_custom_hover'] =  thiss._settings['customPics'][item.sku]['image_url_custom_hover'];
      } catch(e) {}
      // if (
      //   thiss._settings['customPics'] &&
      //   thiss._settings['customPics'][item.sku] &&
      //   thiss._settings['customPics'][item.sku]['image_url_custom']
      // ) {
      //   jsonAllGoods[item.sku]['image_url_custom'] =  thiss._settings['customPics'][item.sku]['image_url_custom'];
      // }
      // if (
      //   thiss._settings['customPics'] &&
      //   thiss._settings['customPics'][item.sku] &&
      //   thiss._settings['customPics'][item.sku]['image_url_custom_hover']
      // ) {
      //   jsonAllGoods[item.sku]['image_url_custom_hover'] =  thiss._settings['customPics'][item.sku]['image_url_custom_hover'];
      // }
    }, this);
  });

  return jsonAllGoods;
}

Shop.prototype.getCats = function () { //DELETE
  var shopCats = {};
  this.shopData.forEach(function (grp) {
    var groupName = grp['name'];
    var groupId = grp['id'];
    shopCats[groupName] = Object.keys(grp['virtual_items']);
  }.bind(this));
  return shopCats;
}


Shop.prototype.getIndexes = function (typeName, typeVal) {
  var arr = [];
  Object.keys(this._data).forEach(function (sku, i) {
    var add = false;
    var item = this._data[sku];
    var typeValItem = item[typeName]
    if (typeValItem === typeVal) {
      add = true;
    };
    if (typeof typeValItem === 'object') {
      if (typeValItem.indexOf(typeVal) >= 0) add = true;
    };
    if (add) arr.push(sku);
  }.bind(this));
  arr = _.compact(arr);
  return arr;
}





function ShopGrid(parentShop, where, typeName, typeVal) {
  this._shop = parentShop;
  this.where = $(where)[0];
  this.typeName = typeName;
  this.typeVal = typeVal;
  this.template = this.getTemplate();
  this._dataArr = this._shop.getIndexes(this.typeName, this.typeVal);
  this.renderedGoods = [];
  this.renderShopGoods(this.where);
}



ShopGrid.prototype.getTemplate = function () {
  return $(this.where).find(':attr(\'^data-good-template\')')[0];
}



ShopGrid.prototype.renderShopGoods = function (localWhere) {
  this._dataArr.forEach(function (sku) {
    var dataItem = this._shop._data[sku];
    var newGood = new ShopGood(this, dataItem, this.template)
    localWhere.appendChild(newGood.element);this.where.appendChild(newGood.element);
  }.bind(this))
}







function ShopGood(_grid, dataItem, template) {
  this._grid = _grid;
  this.dataItem = dataItem;
  this.template = template;

  this._onClick = this._onClick.bind(this);
  // this._onHoverOn = this._onHoverOn.bind(this);
  // this._onHoverOff = this._onHoverOff.bind(this);

  this.element = this.renderShopGood();
}



ShopGood.prototype.renderShopGood = function () {
  var newEl = this.template.cloneNode(true);
  newEl.dataset.idd = this.dataItem.sku;
  newEl.classList.remove('hidden', 'template');
  newEl.dataset.good = null;
  newEl.dataset.goodTemplate = null;
  newEl.dataset.template = null;

  try {
    $(newEl).find('[data-good=\'pic\']')[0].style.backgroundImage = 'url(' + this.dataItem.image_url + ')';
  } catch (e) { };

  var thiss = this;
  try {
    $(newEl).find('[data-good=\'pic_custom\']')[0].style.backgroundImage = 'url(' + this.dataItem.image_url_custom + ')';
    $(newEl).hover(
      function () {
        $(this).find('[data-good=\'pic_custom\']').css('background-image','url(' + thiss.dataItem.image_url_custom_hover + ')')
      },
      function () {
        $(this).find('[data-good=\'pic_custom\']').css('background-image','url(' + thiss.dataItem.image_url_custom + ')')
      },
    );
  } catch (e) { };

  if ($(newEl).find('[data-good=\'pic_bg\']')[0]) {
    $(newEl).find('[data-good=\'pic_bg\']')[0].style.backgroundImage = 'url(' + this.dataItem.image_url + ')';
  }
  $(newEl).find('[data-good=\'name\']')[0].innerHTML = this.dataItem.name;
  $(newEl).find('[data-good=\'desc\']')[0].innerHTML = this.dataItem.description;
  $(newEl).find('[data-good=\'price\']')[0].innerHTML = '$' + this.dataItem.amount;


  // this.onHoverOn = this._onHoverOn;
  // this.onHoverOff = this._onHoverOff;

  // newEl.addEventListener('mouseover', function (evt) {
    //   evt.preventDefault();
    //   if (typeof this.onHoverOn === 'function') {
      //     this.onHoverOn();
      //   }
      // }.bind(this));
      // newEl.addEventListener('mouseout', function (evt) {
        //   evt.preventDefault();
        //   if (typeof this.onHoverOff === 'function') {
          //     this.onHoverOff();
          //   }
          // }.bind(this));
  this.onClick = this._onClick;
  var thiss = this;
  $(newEl).find(':attr(\'^data-kart-add\')').each(function (i, el) {
    el.addEventListener('click', function (evt) {
      var trgt = evt.target;

      if (($(trgt).attr('data-kart-add')) || $(trgt.parentElement).attr('data-kart-add')) {
        evt.preventDefault();
        if (typeof thiss.onClick === 'function') {
          thiss.onClick(trgt);
        }
      }

      // var addBut = $(thiss.element).find(':attr(\'^data-kart-add\')');
      // if (trgt === addBut[0] || trgt.parentElement === addBut[0]) {

      // }
    });
  })



  return newEl;
}



ShopGood.prototype._onClick = function (trgt) {
  // var addBut = $(this.element).find(':attr(\'^data-kart-add\')');
  // if (trgt === addBut[0] || trgt.parentElement === addBut[0]) {
    this._grid._shop.cart.updateGood(this.dataItem['sku'], '+')
  // }
  // if (trgt.classList.contains('k_q_but_minus') || trgt.parentElement.classList.contains('k_q_but_minus')) {
  //   this._shop.cart.updateGood(this.dataItem['sku'], this.q - 1)
  // }
  // if (trgt.classList.contains('k_delete') || trgt.parentElement.classList.contains('k_delete')) {
  //   this._shop.cart.updateGood(this.dataItem['sku'], 0)
  // }
}







Shop.prototype.addButtonsClicks = function (thisShop) {
  this.addButtonsArr.each(function (i, obj) {

    obj.addEventListener('click', function (evt) {
      evt.stopPropagation();
      var el = obj;
      var plusOrMinus = '+';

      var sku = el.dataset.kartAdd; //TODO: сделать, чтобы датасет брался из настроек
      thisShop.cart.updateGood(sku, plusOrMinus);

    })
  });
}

Shop.prototype.collectButtons = function () {
  //TODO: destroy previous
  var butsArr = $(this._settings['cartAddBut']);
  for (var i = 0; i < butsArr.length; i++) {
    var element = butsArr[i];
    console.log(typeof element)
    var newButton = new ShopButton(element, this);
    this.buttons.push(newButton);
    newButton.upDateStatus();
    newButton.onClick = newButton._onClick;
    newButton.onHoverOn = newButton._onHoverOn;
    newButton.addListeners();
  }
}

Shop.prototype.updateButtons = function (sku) {
  for (var i = 0; i < this.buttons.length; i++) {
    var but = this.buttons[i];
    but.upDateStatus();
  }
}

function ShopButton(domEl, parentShop) {
  this._shop = parentShop;
  this.element = domEl;
  this.sku = domEl.dataset.kartAdd;
  this.already = this._shop.cart.checkIfInCart(this.sku);
  this._onClick = this._onClick.bind(this);
  // this._onHoverOn = this._onHoverOn.bind(this);
  // this._onHoverOff = this._onHoverOff.bind(this);
  this.upDateStatus();
}







ShopButton.prototype.onClick = null;
ShopButton.prototype.onHoverOn = null;
// ShopButton.prototype.onHoverOff = null;

ShopButton.prototype._onClick = function (trgt) {
  console.log(this.sku);
  var plusOrMinus = '+';

  //TODO: сделать нормальный метод добавления удаления
  if (this.already && this.element.classList.contains('button_buy--cart')) {
    plusOrMinus = '-';
  }
  if (this.already && this.element.classList.contains('bt_checkout')) {
    plusOrMinus = undefined; //TODO: разобраться с кнопками
  }
  var cartOpenClass = this._shop._settings['cartSettings']['cartOpenClass'];
  // if (trgt.classList.contains(cartOpenClass) || trgt.parentElement.classList.contains(cartOpenClass)) {
  if ($(cartOpenClass).find($(trgt)).length || $(cartOpenClass).find(trgt.childNodes[0]).length) {
    this._shop.cart.openCartAnimation();
  }



  this._shop.cart.updateGood(this.sku, plusOrMinus);
  this._shop.cart.afterChange();
  this.upDateStatus();
};
ShopButton.prototype._onHoverOn = function () {
  console.log(this.sku);
};



ShopButton.prototype.upDateStatus = function () {
  this.already = this._shop.cart.checkIfInCart(this.sku);

  if (typeof this._shop.cart.cartAddAnimation === 'function') {
    this._shop.cart.cartAddAnimation(this.element, this.already);
  }

  if (this.already) { //Default Action
    this.element.classList.add('k_button_already');
  } else {
    this.element.classList.remove('k_button_already');
  }
}




ShopButton.prototype.addListeners = function () {
  var thiss = this;
  this['element'].addEventListener('click', function (evt) {
    evt.preventDefault();
    var trgt = evt.target;
    if (typeof thiss.onClick === 'function') {
      thiss.onClick(trgt);
    }


  });
  // this['element'].addEventListener('mouseover', function (evt) {
  //   evt.preventDefault();
  //   if (typeof thiss.onClick === 'function') {
  //     thiss.onHoverOn();
  //   }
  // }, true);
}


ShopButton.prototype.removeListeners = function () {
  this['element'].removeEventListener('click', this['element']._onClick);
  this['element'].removeEventListener('mouseover', this['element']._onHoverOn);
  // this['element'].removeEventListener('mouseout', this['element']._onClick);
}

























function Cart(parentShop) {
  //* Cart Basics */
  this._shop = parentShop;
  this.cartSettings = this._shop._settings['cartSettings'];
  this.currency = this.cartSettings['currency'] || ['$', 0];
  this.offvalue = false;
  this.paystation = this.cartSettings['paystation'] || {};
  this.renderedGoods = [];
  this.q = 0;
  this.total = 0;
  this.discount = 0;
  this.inCart = []; //передается в ПС
  //* Cart Custom Interactions */
  this.cartAddDiscount = this.cartSettings['cartAddDiscount'] || false;
  this.cartAddAnimation = this.cartSettings['cartAddAnimation'] || false;
  this.ixCartShow = this.cartSettings['cartShowAnim'] || false;
  this.ixCartHide = this.cartSettings['cartHideAnim'] || false;
  this.afterChangeCallback = this.cartSettings['afterChange'] || false;
  //* Cart Elements */
  this.cartIcon = document.querySelector(this.cartSettings['cartIcon']);
  this.cartCounter = $(this.cartSettings['cartCounter']) || false;
  this.cartCont = document.querySelector(this.cartSettings['cartContainer']);
  this.cartWrapper = document.querySelector(this.cartSettings['cartWrapper']);
  this.cartTot = $(this.cartSettings['cartTotal']);
  this.cartСlr = $(this.cartSettings['cartClear']);
  this.checkoutBut = this.cartWrapper.querySelector(this.cartSettings['cartCheckoutBut']);
  this.template = $(this.cartWrapper).find(this.cartSettings['cartItemTemplate'])[0];
  this.cartDiscountPrice = this.cartWrapper.querySelector(this.cartSettings['cartDiscountPrice']);
  //* Cart Misc */
  this.dataLoad();
  this.iconCount();
  this.iconCount();
  this.addClearClick();
  this.addCheckOutClick();
  this.showCartClick();
  this.calculateTotal();
  this.cartOpeners();
  this.cartClosers();
  this.psInit();
}




Cart.prototype.cartOpeners = function () {
  var openButtons = $(this.cartSettings['cartOpenClass']);
  var thiss = this;
  $.each(openButtons, function (i, item) {
    item.addEventListener('click', function (evt) {
      this.openCartAnimation();
    }.bind(this), true);
  }.bind(this));
}


Cart.prototype.openCartAnimation = function () {
  var ix_cartShow = this.ixCartShow || {
    'stepsA': [{
      'opacity': 1,
      'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
      'x': '0px',
      'y': '0px',
    }]
  };
  if (typeof ix_cartShow !== 'object') return;
  this.cartWrapper.classList.add('shown');
  this.cartWrapper.classList.add('shown');
  this.cartWrapper.style.transform = '';
  ix.run(ix_cartShow, this.cartWrapper.querySelector('.kb'));
  ix.run(ix_cartShow, this.cartWrapper.querySelector('.kz'));
}

Cart.prototype.cartClosers = function () {
  var closeButtons = $(this.cartSettings['cartCloseClass']);
  var thiss = this;
  $.each(closeButtons, function (i, item) {
    item.addEventListener('click', function (evt) {
      this.closeCartAnimation();
    }.bind(this), true);
  }.bind(this));
}


Cart.prototype.closeCartAnimation = function () {
  var ix_cartHide = this.ixCartHide || {
    'stepsA': [{
      'transition': 'transform 200 ease 0',
      'x': '900px',
      'y': '0px',
      'z': '0px'
    }],
    'stepsB': []
  }
  var ix_cartHide2 = {
    'stepsA': [{
      'opacity': 0,
      'transition': 'opacity 200 ease 0'
    }],
    'stepsB': []
  }

  if (typeof ix_cartHide === 'object') { //Чтобы запретить анимацию на проекте
    ix.run(ix_cartHide, this.cartWrapper.querySelector('.kb'));
    ix.run(ix_cartHide2, this.cartWrapper.querySelector('.kz'));
    setTimeout(function () {
      this.cartWrapper.classList.remove('shown');
    }.bind(this), 200);
  }
}



Cart.prototype.psInit = function () {
  var psInit = this.cartSettings['psInit'];
  psInit();
}




Cart.prototype.afterChange = function () {
  if (this.afterChangeCallback) this.afterChangeCallback(this);
}





Cart.prototype.getCurrency = function () {
  return this.currency[0];
}


Cart.prototype.checkIfInCart = function (sku) {
  var foundIndex = this.data2skus(this.inCart).indexOf(sku);
  return foundIndex < 0? false : true;
}



Cart.prototype.addCheckOutClick = function () {
  this.checkoutBut.addEventListener('click', function () {

    var access_data_cart = this.cartSettings.paystation['access_data'];
    access_data_cart['user']['attributes']['promo'] = this.offvalue;
    // access_data_cart.
    //  var sku = $(this).data('sku');
    access_data_cart.purchase.virtual_items.items = this.inCart;

    // var widgetInstance = XPayStationWidget.create(options1);
    var options = {
      'access_data': access_data_cart,
      'lightbox': this.cartSettings.paystation['lightbox']
    };

    var thiss = this;
    if (debug) {
      thiss.debugCart(); //TODO убрать
      console.log('options = ', options);
    }
    setTimeout(function() { //Чтобы скролл окна убрать
      XPayStationWidget.init(options);
      XPayStationWidget.open();
    }, 50);
  }.bind(this));
}






Cart.prototype.addClearClick = function () {
  var thiss = this;
  this.cartСlr.each(function (i, oneClr) {
    oneClr.addEventListener('click', function () {
      thiss.clearCart();
    }.bind(thiss));
  })
}






Cart.prototype.showCartClick = function () {
  this.cartIcon.addEventListener('click', function () {
    this.afterChange(this);
  }.bind(this));
}






Cart.prototype.calculateTotal = function () {
  var tot = [];
  var amnt = [];
  for (var i = 0; i < this.inCart.length; i++) {
    var el = this.inCart[i];
    tot.push(parseFloat(this._shop._data[el['sku']]['amount'], 10) * el['amount']);
    amnt.push(el['amount']);
  }
  tot = tot.reduce(function(sum, current) {
    return sum + current;
  }, 0);
  this.q = amnt.reduce(function(sum, current) {
    return sum + current;
  }, 0);
  this.addDiscount();
  this.total = tot - this.discount;
  if (this.cartAddDiscount) {
    this.cartDiscountPrice.textContent = '–' + this.xsollaCartPrice(this.discount);
  };
  this.iconCount();

  var tot = this.total;
  var thiss = this;
  this.cartTot.each(function (i, oneTot) {
    oneTot.textContent = thiss.xsollaCartPrice(tot);
   })


  this.dataSave();
  this._shop.updateButtons();
  this.afterChange();
}






Cart.prototype.clearCart = function () {
  for (var i = 0; i < this.renderedGoods.length; i++) {
    var oneGood = this.renderedGoods[i];
    oneGood.destroy();
  }
  this.renderedGoods.length = 0;
  this.inCart.length = 0;
  this.calculateTotal();
  this.iconCount();
}






Cart.prototype.iconCount = function () { //TODO: нормально найти элементы

  var q = this.q;

  this.cartCounter.each(function(i, oneCounter) {
    oneCounter.textContent = q;
  }).bind(this);

  var variants = this.cartSettings['indicatorShown'];
  switch (variants) {
    case 'auto':
      if (!this.q) {
        this.cartIcon.classList.remove('shown');
      } else {
        this.cartIcon.classList.add('shown');
      }
      break;
    case 'shown':
      this.cartIcon.classList.add('shown');
      break;
  }

}






Cart.prototype.debugCart = function () {
  for (var i = 0; i < this.inCart.length; i++) {
    console.log(this.inCart[i]);
  }
}






Cart.prototype.updateGood = function (sku, newAmount) {
  //this.q++; //общее кол-во товаров в корзине

  var ind = this.data2skus(this.inCart).indexOf(sku);
  var changedDataItem = this.inCart[ind];

  if (newAmount === '+') {
    if (ind < 0) {
      newAmount = 1;
    } else {
      newAmount = changedDataItem['amount'] + 1;
    }
  }
  if (newAmount === '-') {
    if (ind < 0) {
      newAmount = 0;
    } else {
      newAmount = changedDataItem['amount'] - 1;
    }
  }

  var quanityChanged = false;
  if (this.inCart.length) {
    //если уже есть и надо проверить
    for (var i = 0; i < this.inCart.length; i++) {
      var dataItemOne = this.inCart[i];
      //If there's already a hat of this type
      if (dataItemOne['sku'] === sku) {
        if (newAmount) {
          dataItemOne['amount'] = newAmount;
          this.changeAmountHtml(sku, newAmount);
        } else {
          this.changeAmountHtml(sku, 0);
          delete this.inCart[i];
        }
        quanityChanged = true;
      }
      this.inCart = _.compact(this.inCart);
    }
  } else {
    for (var i = 0; i < this.renderedGoods.length; i++) {
      this.changeAmountHtml(this.renderedGoods[i]['dataItem']['sku'], 0)
      console.log('deleted');
    }
  }
  if (!quanityChanged) {
    this.inCart.push({ 'sku': sku, 'amount': 1 })
    this.drawOneGood(sku, 1);
  }
  this.iconCount();
  this.calculateTotal();
}






Cart.prototype.dataLoad = function () {
  var projectId = this.cartSettings.paystation.access_data.settings.project_id;
  this.inCart = localStorage.getItem('xsolla_cart' + projectId);
  this.inCart = JSON.parse(this.inCart);
  if (this.inCart === null) {
    this.inCart = [];
  }
  this.data2render();
  // this.updateGood();
}






Cart.prototype.dataSave = function () {
  var projectId = this.cartSettings.paystation.access_data.settings.project_id;
  console.log('projectId = ', projectId);
  localStorage.setItem('xsolla_cart' + projectId, JSON.stringify(this.inCart))
}






Cart.prototype.data2skus = function (dataArr) {
  var dataSkus = false;
  if (dataArr) {
    dataSkus = []; //Collect all skus from data

    for (var u = 0; u < dataArr.length; u++) {
      var oneSku = dataArr[u]['sku'];
      dataSkus.push(oneSku);
    }
  }
  return dataSkus;
}






Cart.prototype.render2data = function () {
  var dataFromRender;
  if (this.renderedGoods && this.renderedGoods.length) {
    dataFromRender = [];
    for (var i = 0; i < this.renderedGoods.length; i++) {
      var el = this.renderedGoods[i];
      dataFromRender.push(el.dataItem);
    }
  }
  return dataFromRender;
}

Cart.prototype.data2render = function () { //TODO: delete
  if (!this.inCart.length) return;
  for (i = 0; i < this.inCart.length; i++) {
    var newEl = new RenderedGood(this, this.inCart[i], this.template);
    this.renderedGoods.push(newEl)
    this.appearOneGood(newEl);
  }
}





Cart.prototype.drawAll = function () {
  for (var i = 0; i < this.renderedGoods.length; i++) {
    var newEl = this.renderedGoods[i];
    this.appearOneGood(newEl);
  }
}






Cart.prototype.drawOneGood = function (sku, amount) {
  var newEl = new RenderedGood(this, {'sku': sku, 'amount': amount}, this.template);
  this.renderedGoods.push(newEl)
  this.appearOneGood(newEl);
}



Cart.prototype.appearOneGood = function (newEl) {
  newEl.updateGoodControls();
  newEl.element.style.height = 0;
  newEl.element.style.opacity = 0;
  this.cartCont.appendChild(newEl.element);
  ix.run(ixKItemAppear, newEl.element);
}

Cart.prototype.disAppearOneGood = function (newEl) {
  // newEl.element.style.height = 100;
  $(newEl['element']).css('pointer-events', 'none');
  ix.run(ixKItemDisAppear, newEl.element);
  var thisEl = newEl.element;
  setTimeout(function () {
    thisEl.parentElement.removeChild(thisEl);
  }, 100);
}



Cart.prototype.changeAmountHtml = function (goodSku, newAmount) {
  var numb = this.data2skus(this.inCart).indexOf(goodSku);
  if (numb < 0) { return };
  if (newAmount) {
    this.renderedGoods[numb]['q'] = newAmount;
    this.renderedGoods[numb].updateQ();
  } else {
    this.deleteElement(this.renderedGoods[numb]);
  }
}








Cart.prototype.deleteElement = function (renderedGood) {
  var indToDel = this.renderedGoods.indexOf(renderedGood);
  if (indToDel < 0) return;
  delete this.renderedGoods[indToDel];
  this.renderedGoods = _.compact(this.renderedGoods);
  renderedGood.destroy();
}






Cart.prototype.removeGood = function (renderedGood) {
  this.q--;
  var skuToDelete = renderedGood.good['sku'];
  var changed = false;
  for (var u = 0; u < this.inCart.length; u++) {
    var oneSku = this.inCart[u];
    if (oneSku['sku'] === skuToDelete && !changed) { //Find sku in cart
      oneSku['amount']--;
      changed = true;
      renderedGood.q--;
      renderedGood.element = renderedGood.renderGood();
    }
  }
}






Cart.prototype.addDiscount = function () {
  if (!this.cartAddDiscount) {
    return;
  }
  var discountCallback = this.cartAddDiscount(this.q);
  this.discount = discountCallback[0];
  this.offvalue = discountCallback[1];
  //Прописать полученную скидку в поле скидка
  if (this.cartAddDiscount) {
    this.cartDiscountPrice.textContent = '–' + this.xsollaCartPrice(this.discount);
  };
}







///*********************************///
///*********************************///
///*********************************///
///*********************************///
///*********************************///
///*********************************///








function RenderedGood(parentCart, dataItem, template) {
  this._cart = parentCart;
  this.template = template;
  this.dataItem = dataItem;
  this.sku = this.dataItem['sku'];
  this.q = this.dataItem['amount'];
  this.image_url = this._cart._shop._data[this.sku]['image_url'];
  this.name = this._cart._shop._data[this.sku]['name'];
  this.desc = this._cart._shop._data[this.sku]['description'];
  this.price = this._cart._shop._data[this.sku]['amount'];
  this.element = this.renderGood();

  this._onClick = this._onClick.bind(this);
  this._onHoverOn = this._onHoverOn.bind(this);
  this._onHoverOff = this._onHoverOff.bind(this);
  this.updateGoodControls();
}






RenderedGood.prototype.renderGood = function () {
  var el = this.template.cloneNode(true);
  el.classList.remove('hidden');
  el.dataset.template = 'false';

  var goodPrice = $(el).find('*[data-good="price"]');
  var goodPic = $(el).find('*[data-good="pic"]');
  var goodName = $(el).find('*[data-good="name"]');
  var goodDesc = $(el).find('*[data-good="desc"]');


  goodPrice[0].innerText = this._cart.xsollaCartPrice(this.price * this.q);
  goodPic[0].style.backgroundImage = 'url(\'' + this.image_url + '\')';
  goodName[0].innerText = this.name;
  goodDesc[0].innerText = this.desc;
  el.querySelector('.k_q_control').textContent = this.q;
  this.onClick = this._onClick;
  this.onHoverOn = this._onHoverOn;
  this.onHoverOff = this._onHoverOff;

  el.addEventListener('mouseover', function (evt) {
    evt.preventDefault();
    if (typeof this.onHoverOn === 'function') {
      this.onHoverOn();
    }
  }.bind(this));
  el.addEventListener('mouseout', function (evt) {
    evt.preventDefault();
    if (typeof this.onHoverOff === 'function') {
      this.onHoverOff();
    }
  }.bind(this));
  el.addEventListener('click', function (evt) {
    var trgt = evt.target;
    evt.preventDefault();
    if (typeof this.onClick === 'function') {
      this.onClick(trgt);
    }
  }.bind(this));

  return el;
}






var ix = ix || Webflow.require('ix');

var ixKshowControls = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'opacity 200ms ease 0ms',
  }]
};

var ixHideControls = {
  'stepsA': [{
    'opacity': 0,
    'transition': 'opacity 200ms ease 0ms',
  }]
};

var ixKPlus = ixKPlus || {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'scaleX': 1,
    'scaleY': 1,
    'scaleZ': 1,
    'y': '-26px',
  }]
};

var ixKMinus = ixKMinus || {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'scaleX': 1,
    'scaleY': 1,
    'scaleZ': 1,
    'y': '26px',
  }]
};

var ixKPlusMinusOff = ixKPlusMinusOff || {
  'stepsA': [{
    'opacity': 0,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'scaleX': 1,
    'scaleY': 1,
    'scaleZ': 1,
    'y': '0px',
  }]
};


var ixKItemAppear = ixKItemAppear || {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 100ms ease 0ms, opacity 100ms ease 0ms',
    'height': '100px'
  }]
};

var ixKItemDisAppear = ixKItemDisAppear || {
  'stepsA': [{
    'opacity': 0,
    'transition': 'transform 100ms ease 0ms, opacity 100ms ease 0ms',
    'height': '0px'
  }]
};




RenderedGood.prototype._onHoverOn = function () {
  if (isMobile) return;
  if (!this.element.classList.contains('k_q')) {
    //Vertical controls
    ix.run(ixKPlus, this.element.querySelectorAll('.k_q_but_plus'));
    ix.run(ixKMinus, this.element.querySelectorAll('.k_q_but_min'));

    //Horizontal
    // var buttons = this.element.querySelectorAll('.k_q_but');
    // collectionDo(buttons, function (el) {
    //   ix.run(ixKshowControls, el);
    // });
  }
}






RenderedGood.prototype._onHoverOff = function () {
  if (isMobile) return;
  if (!this.element.classList.contains('item_active')) {

    // Vertical
    var buttons = this.element.querySelectorAll('.k_q_but');
    collectionDo(buttons, function (el) {
      ix.run(ixKPlusMinusOff, el);
    });
    //Horizontal
    // var buttons = this.element.querySelectorAll('.k_q_but');
    // collectionDo(buttons, function (el) {
    //   ix.run(ixHideControls, el);
    // });
  }
}






RenderedGood.prototype._onClick = function (trgt) {
  if (trgt.classList.contains('k_q_but_plus') || trgt.parentElement.classList.contains('k_q_but_plus')) {
    this._cart.updateGood(this.dataItem['sku'], this.q + 1)
  }
  if (trgt.classList.contains('k_q_but_minus') || trgt.parentElement.classList.contains('k_q_but_minus')) {
    this._cart.updateGood(this.dataItem['sku'], this.q - 1)
  }
  if (trgt.dataset.kartDelete || trgt.parentElement.dataset.kartDelete) {
    this._cart.updateGood(this.dataItem['sku'], 0)
  }
}






RenderedGood.prototype.destroy = function () {
  this['element'].removeEventListener('click', this['element']._onClick);
  this['element'].removeEventListener('mouseover', this['element']._onClick);
  this['element'].removeEventListener('mouseout', this['element']._onClick);
  this._cart.disAppearOneGood(this);
}






RenderedGood.prototype.updateGoodControls = function () {
  //Change Minus to Delete
  if (this.q > 1) {
    this['element'].querySelector('.k_delete').classList.add('disabled');
    this['element'].querySelector('.k_q_but_minus').classList.remove('disabled');
  } else {
    this['element'].querySelector('.k_delete').classList.remove('disabled');
    this['element'].querySelector('.k_q_but_minus').classList.add('disabled')
  }
}






RenderedGood.prototype.updateQ = function () {
  this['element'].querySelector('.k_q_control').textContent = this['q'];
  this['element'].querySelector('.k_price').textContent = this._cart.xsollaCartPrice(this.price * this.q);
  this.updateGoodControls();
  var ix_qChange_1 = ix_qChange_1 || {
    'stepsA': [{
      'opacity': 1,
      'scale': 1.2,
      'transition': 'transform 100ms ease 0ms, opacity 200ms ease 0ms',
    }]
  };
  var ix_qChange_0 = ix_qChange_0 || {
    'stepsA': [{
      'opacity': 0.3,
      'scale': 1,
      'transition': 'transform 100ms ease 0ms, opacity 200ms ease 0ms',
    }]
  };
  var qGlow = this.element.querySelector('.k_q_glow');
  qGlow.classList.remove('hidden');
  ix.run(ix_qChange_1, qGlow);
  setTimeout(function() {
    ix.run(ix_qChange_0, qGlow);
    setTimeout(function() {
      qGlow.classList.add('hidden');
    }, 100);
  }, 100);

}


Cart.prototype.xsollaCartPrice = function (price) {
  price = Math.round(price * 100) / 100;
  price = price.toString();
  var priceCheck = price.split('.');
  if (priceCheck && priceCheck.length === 2 && priceCheck[1].length === 1) {
    price = price + '0';
  }
  var formattedPrice;
  if (this.currency[1] === 0) {
    formattedPrice = this.getCurrency() + price;
  } else {
    formattedPrice = price + this.getCurrency();
  }
  return formattedPrice;
}