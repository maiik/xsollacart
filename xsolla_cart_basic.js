

var atari = {
  'atari_fuji': {
    'name': 'Fuji Blackout',
    'image_url': 'https://daks2k3a4ib2z.cloudfront.net/59c064a86eb47c0001393eae/59cafbf7899ef10001054c3c_200_fuji.png',
    'price': 129.99,
    'physical': true,
  },
  'atari_blade_runner': {
    'name': 'Blade Runner 2049',
    'image_url': 'https://daks2k3a4ib2z.cloudfront.net/59c064a86eb47c0001393eae/59cafbf75fbcd60001319745_200_blader.png',
    'price': 139.99,
    'physical': true,
  },
  'atari_royal_blue': {
    'name': 'Royal Blue/White',
    'image_url': 'https://daks2k3a4ib2z.cloudfront.net/59c064a86eb47c0001393eae/59c70c0dadda34000132a224_blue_01.jpg',
    'price': 129.99,
    'physical': true,
  },
  'atari_black': {
    'name': 'Black/White',
    'image_url': 'https://daks2k3a4ib2z.cloudfront.net/59c064a86eb47c0001393eae/59c70c0a84228d0001781820_black_01.jpg',
    'price': 129.99,
    'physical': true,
  },
}


var ix_But_addToCart_1 = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'x': '-102px',
  }]
};

var ix_But_addToCart_0 = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'x': '0px',
  }]
};


var ix_But_addBuy_1 = {
  'stepsA': [{
    'opacity': 0,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
  }]
};

var ix_But_addBuy_0 = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
  }]
};


function cartAddAnimation(el, already) {
  var butCart = el.parentElement.parentElement.querySelector('.ix_bt_cart');
  var butBuy = el.parentElement.parentElement.querySelector('.ix_bt_buy');
  var carticon = el.parentElement.parentElement.querySelector('.ix_bt_carticon');
  var crtdelicon = el.parentElement.parentElement.querySelector('.ix_bt_crtdelicon');

  if (el.classList.contains('k_li_upsale_item')) return;
  if (already) {
    // this.element.parentElement.parentElement.classList.add('already');
    ix.run(ix_But_addToCart_1, butCart);
    ix.run(ix_But_addBuy_1, butBuy);
    carticon.classList.add('hidden');
    crtdelicon.classList.remove('hidden');
    // setTimeout(function() {
      butBuy.classList.add('invisible');
    // }, 100);
  } else {
    // this.element.parentElement.parentElement.classList.remove('already');
    ix.run(ix_But_addToCart_0, butCart);
    ix.run(ix_But_addBuy_0, butBuy);
    crtdelicon.classList.add('hidden');
    carticon.classList.remove('hidden');
    // setTimeout(function() {
      butBuy.classList.remove('invisible');
    // }, 200);
  }
}

function cartAddDiscount(q) { //TODO: сделать подсчет скидки более универсальным, передавать не кол-во, а состав корзины
  var discount = 0;
  var offvalue = false;
  var discountBlock = document.querySelector('#kart_discount');
  $(discountBlock).find('.active').removeClass('active').addClass('inactive');
  // if (this.cartCont.querySelector('#kart_discount')) {
  if (q < 2) {
    // this.cartCont.removeChild(this.cartCont.querySelector('#kart_discount'));
    discount = 0;
    offvalue = false;
  }
  if (q >= 2 && q < 4 ) {
    discount = 15;
    $(discountBlock).find('[data-cart-discount="' + discount + '"]').addClass('active').removeClass('inactive');
    offvalue = '15OFF';
  }
  if (q >= 4) {
    discount = 25;
    // $(this.discountBlock).find('.active').addClass('active');
    $(discountBlock).find('[data-cart-discount="' + discount + '"]').addClass('active').removeClass('inactive');
    offvalue = '25OFF';
  }

  return [discount, offvalue];
}





var shopSettings =
  {
    //Shop Settings
    'shopData': [], //Data from Xsolla Merchant. Ex.: http://xsolla.maiik.ru/phoenix_point/atari_json.html
    'cartAddBut': ':attr(\'^data-cart-add\')',
    //Cart Settings
    'cartSettings': {
      'indicatorShown': 'auto', //auto
      'afterChange': typeof kCheckOutPosition !== 'undefined' ? kCheckOutPosition : null, //Callback when something changed in the cart
      'cartAddAnimation': typeof cartAddAnimation !== 'undefined' ? cartAddAnimation : null,
      'cartAddDiscount': typeof cartAddDiscount !== 'undefined' ? cartAddDiscount : null,
      'paystation': {
        'access_data': {
          'user': {
            'attributes': {
              'promo': false,
            }
          },
          'settings': {
            'project_id': 22887,
            'shipping_enabled': true
          },
          'purchase': {
            'virtual_items': {
              'items': []
            }
          }
        },
        'lightbox': {
          'width': '740px',
          'height': '685px',
          'spinner': 'round',
          'spinnerColor': '#cccccc',
        }
      },
      'cartWrapper': '.k',
      'cartItemTemplate': ':attr(\'^data-kart-template\')',
      'cartIcon': '#top_cart',
      'cartContainer': '#kart',
      'cartTotal': '#kart_tot',
      'cartCheckoutBut': '.button_buy--checkout',
      'cartDiscountPrice': '#kart_tot_discount',
      'cartClear': ':attr(\'^data-kart-clear\')',
      // 'currency': ['₽', 1],
      'currency': ['$', 0],
      'psInit': function () {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = '//static.xsolla.com/embed/paystation/1.0.7/widget.min.js';
        var head = document.getElementsByTagName('head')[0];
        head.appendChild(s);
      }
    }
  };





function getVirtualItems(access_data) {
  $.ajax({
      url: 'https://secure.xsolla.com/paystation2/api/virtualitems/',
      data: {
          access_data: JSON.stringify(access_data)
      },
      method: 'post',
      dataType: 'json',
      beforeSend: function () {

      },
      success: function (items_data) {
          console.log(items_data);
        //   document.querySelector('#jsn').innerText = JSON.stringify(data['groups'][0]['virtual_items']);
          // document.querySelector('#jsn').innerHTML = syntaxHighlight(data['groups'][0]['virtual_items']);
        //   document.querySelector('.prettyprint').innerHTML = JSON.stringify(data['groups'][0]['virtual_items']);
          shopSettings['shopData'] = items_data['groups'];
          shopSettings.cartSettings.paystation.access_data.project_id = access_data.project_id;
          var shop = new Shop(shopSettings);
      }
  });
}






var access_data_atari = {
  "settings": {
      "project_id": 22887
  }
};


var access_data_phoenix = {
  "settings": {
      "project_id": 22877
  }
};

// getVirtualItems(access_data_atari);
getVirtualItems(access_data_atari);



